const express = require('express');
const router = express.Router();
const config = require('../../../../package.json');

//HOME PAGE ROUTER
router.get('/', (req, res, next) => {
    res.render('index', {
        title: config.name
    });
});

//SUBPAGE ROUTER
router.get('/:template', (req, res, next) => {
    title = req.params.template;
    if (title !== 'admin') {
        res.render(title, {
            title: title + ' - ' + config.name
        });
    } else {
        next();
    }
});



module.exports = router;