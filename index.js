//DEPENDENCIES
const express = require('express');
const exphbs = require('express-handlebars');
const path = require('path');
const Ouch = require('ouch');
const bodyParser = require('body-parser');

//CONFIG
const config = require('../../../package.json');

//ROUTES
const routes = require('./routes/index');

//APP
const app = express();

//TEMPLATES
app.set('views', path.join(__dirname, '../../../views/default'));
app.engine('.hbs', exphbs({extname: '.hbs', defaultLayout: 'main', partialsDir: 'views/default/partials'}));
app.set('view engine', '.hbs');

//PUBLIC DIRECTORY
app.use(express.static(path.join(__dirname, '../../../public')));

//BODY PARSER
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

//ROUTER
app.use('/', routes);

//ERROR
app.use(function (req, res, next) {
    throw new Error("Not found");
});

app.use(function (err, req, res, next) {
    (new Ouch()).pushHandler(
            new Ouch.handlers.PrettyPageHandler()
            ).handleException(err, req, res,
            function () {
                console.log('Error handled' + err);
            }
    );
});

//EXPORT
module.exports = {
    app: app,
    config: config
};